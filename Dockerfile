#Imagen base
FROM node:latest

#Directorio de la aplicacion en el contenedor
WORKDIR /app

#copiado de archivos
ADD . /app

#Dependencias
RUN npm install

#Puerto que expongo
EXPOSE 3000

#comando
cmd ["npm", "start"]
